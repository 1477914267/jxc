package com.atguigu.jxc.vo;

import com.atguigu.jxc.entity.Goods;
import lombok.Data;

/**
 * ClassName: GoodsVo
 * Package: com.atguigu.jxc.vo
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 10:09
 * @Version 1.0
 */
@Data
public class GoodsVo extends Goods{


    private String goodsTypeName;

    private Integer saleTotal;




}

package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: SupplierController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 15:43
 * @Version 1.0
 */
@Api(tags = "供应商管理")
@RestController
@RequestMapping("supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;


    @ApiOperation("分页查询供应商")
    @PostMapping("list")
    public Map<String, Object> getSupplierList(@RequestParam Integer page,
                                               @RequestParam Integer rows,
                                               @RequestParam(required = false) String supplierName) {

        Map<String, Object> map = supplierService.getSupplierList(page, rows, supplierName);

        return map;
    }

    @PostMapping("save")
    @ApiOperation("供应商添加或修改")
    public ServiceVO saveOrUpdate(Supplier supplier,@RequestParam(required = false) Integer supplierId) {

        supplierService.saveOrUpdate(supplier,supplierId);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @ApiOperation("删除供应商")
    @PostMapping("delete")
    public ServiceVO delete(@RequestParam String ids){

        supplierService.remove(ids);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }


}

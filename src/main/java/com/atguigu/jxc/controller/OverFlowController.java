package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DrawImageService;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @description 商品报损
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverFlowController {

    @Autowired
    private OverFlowService overFlowService;

    /**
     * 保存报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("save")
    public ServiceVO save(OverflowList overflowList,
                          @RequestParam("overflowListGoodsStr") String overflowListGoodsStr, HttpServletRequest request){

        HttpSession session = request.getSession();

        User user = (User)session.getAttribute("currentUser");


        overFlowService.save(overflowList,overflowListGoodsStr,user);



        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("list")
    public Map<String,Object> getOverFlowList(@RequestParam String sTime,
                                            @RequestParam String eTime , HttpSession session){

        User user = (User) session.getAttribute("currentUser");
        Map<String,Object> map=  overFlowService.getoverFlowList(sTime,eTime,user);

        return  map;
    }

    /**
     * 报溢单商品查询
     * @param overflowListId
     * @return
     */
    @PostMapping("goodsList")
    public Map<String,Object> goodsList(@RequestParam Integer overflowListId){
        Map<String,Object> map= overFlowService.goodsList(overflowListId);

        return map;
    }



}

package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.Service;
import java.util.Map;

/**
 * ClassName: DamageController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 21:22
 * @Version 1.0
 */

/**
 * 商品报损
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageController {

    @Autowired
    DamageService damageService;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("save")
    public ServiceVO save(  DamageList damageList,
                          @RequestParam String damageListGoodsStr, HttpServletRequest request){

        HttpSession session = request.getSession();

        User user = (User)session.getAttribute("currentUser");


        damageService.save(damageList,damageListGoodsStr,user);



        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("list")
    public Map<String,Object> getDamageList(@RequestParam String sTime,
                                            @RequestParam String eTime ,HttpSession session){

        User user = (User) session.getAttribute("currentUser");
        Map<String,Object> map=  damageService.getDamageList(sTime,eTime,user);

        return  map;
    }

    /**
     * 报损单商品查询
     * @param damageListId
     * @return
     */
    @PostMapping("goodsList")
    public Map<String,Object> goodsList(@RequestParam Integer damageListId){
       Map<String,Object> map= damageService.goodsList(damageListId);

       return map;
    }





}

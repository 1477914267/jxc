package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * ClassName: CustomerController
 * Package: com.atguigu.jxc.controller
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 17:33
 * @Version 1.0
 */
@RestController
@Api(tags = "客户管理")
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    CustomerService CustomerService;



    @ApiOperation("客户列表分页（名称模糊查询）")
    @PostMapping("list")
    public Map<String, Object> getSupplierList(@RequestParam Integer page,
                                               @RequestParam Integer rows,
                                               @RequestParam(required = false) String customerName) {

        Map<String, Object> map = CustomerService.getCustomerList(page, rows, customerName);

        return map;
    }

    @PostMapping("save")
    @ApiOperation("客户添加或修改")
    public ServiceVO saveOrUpdate(Customer customer, @RequestParam(required = false) Integer customerId) {

        CustomerService.saveOrUpdate(customer,customerId);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @ApiOperation("客户删除（支持批量删除）")
    @PostMapping("delete")
    public ServiceVO delete(@RequestParam String ids){

        CustomerService.remove(ids);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

}

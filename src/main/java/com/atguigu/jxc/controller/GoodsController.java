package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Tag;
import javafx.beans.binding.ObjectExpression;
import org.apache.ibatis.annotations.One;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */

@RequestMapping("/goods")
@RestController

@Api(tags = "商品信息Controller")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */


    @ApiOperation("分页查询商品库存信息")
    @PostMapping("/listInventory")
    public Map<String,Object> listInventory(@RequestParam Long page,
                                            @RequestParam Long rows,
                                            @RequestParam(required = false) String codeOrName,
                                            @RequestParam(required = false) String goodsTypeId){

      Map<String,Object> map= goodsService.pageGoodsList(page,rows,codeOrName,goodsTypeId);

      return map;
    }


    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */

    @ApiOperation("分页查询商品信息")
    @PostMapping("list")
    public Map<String,Object> goodsList(@RequestParam Long page,
                                        @RequestParam Long rows,
                                        @RequestParam(required = false) String goodsName,
                                        @RequestParam(required = false) Long goodsTypeId){
       Map<String,Object> map= goodsService.getGoodsList(page,rows,goodsName,goodsTypeId);

       return map;
    }

    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("save")
    public ServiceVO saveOrUpdate(Goods goods
            ,@RequestParam(required = false) Integer goodsId){

        goodsService.saveOrUpdate(goods,goodsId);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }


    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */

    @PostMapping("delete")
    public ServiceVO delete(@RequestParam Integer goodsId){
      ServiceVO serviceVO=  goodsService.delete(goodsId);

        return serviceVO;
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(
            @RequestParam Long page,
            @RequestParam Long rows,
            @RequestParam(required = false) String nameOrCode){
       Map<String, Object> map=goodsService.getNoInventoryQuantity(page,rows,nameOrCode);

       return map;
    }



    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(
            @RequestParam Long page,
            @RequestParam Long rows,
            @RequestParam(required = false) String nameOrCode){
        Map<String, Object> map=goodsService.getHasInventoryQuantity(page,rows,nameOrCode);

        return map;
    }




    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */

    @PostMapping("saveStock")
    public ServiceVO saveStock(@RequestParam Integer goodsId,
                               @RequestParam Integer inventoryQuantity,
                               @RequestParam Double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */

    @PostMapping("deleteStock")
    public ServiceVO deleteStock(@RequestParam Integer goodsId){
      return   goodsService.delete(goodsId);

    }


    /**
     * 查询库存报警商品信息
     * @return
     */

    @PostMapping("listAlarm")
    public Map<String,Object> listAlarm(){
        Map<String,Object> map=  goodsService.listAlarm();

        return map;
    }

}

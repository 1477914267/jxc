package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.rmi.CORBA.Util;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: UnitServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 18:11
 * @Version 1.0
 */
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    UnitDao unitDao;

    /**
     * 查询所有商品单位
     * @return
     */
    @Override
    public Map<String, Object> unitList() {

       List<Unit> unitList= unitDao.unitList();

       Map<String, Object> map = new HashMap<>();

       map.put("rows",unitList);

        return map;
    }
}

package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ClassName: DamageService
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 21:26
 * @Version 1.0
 */
@Service
public class DamageServiceImpl implements DamageService {

    @Autowired
    DamageListDao damageListDao;

    @Autowired
    DamageListGoodsDao damageListGoodsDao;

    @Autowired
    UserDao userDao;


    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @param user
     */
    @Override
    public void save(DamageList damageList, String damageListGoodsStr, User user) {

        //将字符串转换为对象
        Type type = new TypeToken<List<DamageListGoods>>() {
        }.getType();
        List<DamageListGoods> list = new Gson().fromJson(damageListGoodsStr, type);

        damageList.setUserId(user.getUserId());

        damageListDao.save(damageList);

        list.stream()
                .forEach(item ->
                        item.setDamageListId(damageList.getDamageListId())
                );
        damageListGoodsDao.batchSave(list);


    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> getDamageList(String sTime, String eTime, User user) {


        List<DamageList> damageLists = damageListDao.getDamageList(sTime, eTime);

        damageLists.stream()
                .forEach(item ->
                        item.setTrueName(userDao.getUserById(item.getUserId()).getTrueName())
                );

        Map<String, Object> map = new HashMap<>();

        map.put("rows", damageLists);

        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

      List<DamageListGoods> damageListGoodsList=  damageListGoodsDao.goodsList(damageListId);

        Map<String, Object> map = new HashMap<>();

        map.put("rows", damageListGoodsList);

        return map;







    }
}

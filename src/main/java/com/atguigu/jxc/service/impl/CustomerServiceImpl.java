package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: CustomerServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 17:35
 * @Version 1.0
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;

    /**
     * 客户列表分页（名称模糊查询）
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {

        page = (page - 1) * rows;

        Map<String, Object> ParamMap = new HashMap<>();

        ParamMap.put("page", page);
        ParamMap.put("rows", rows);
        ParamMap.put("customerName", customerName);

        List<Customer> customerList = customerDao.getCustomerList(ParamMap);

        HashMap<String, Object> supplierMap = new HashMap<>();

        supplierMap.put("rows", customerList);
        supplierMap.put("total", customerList.size());

        return supplierMap;

    }

    @Override
    public void saveOrUpdate(Customer customer, Integer customerId) {

        if (customerId == null) {

            customerDao.save(customer);

        } else {
            customer.setCustomerId(customerId);
            customerDao.update(customer);

        }
    }

    @Override
    public void remove(String idStr) {

        List<Integer> ids = Arrays.stream(idStr.split(",")).map(item -> Integer.parseInt(item)).collect(Collectors.toList());

        customerDao.batchDel(ids);



    }
}

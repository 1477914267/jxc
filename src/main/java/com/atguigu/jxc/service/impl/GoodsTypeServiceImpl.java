package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;


    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     */
    @Override
    public void saveType(String goodsTypeName, Integer pId) {

       GoodsType goodsType= goodsTypeDao.getById(pId);

       if(goodsType.getGoodsTypeState()==0){

           goodsType.setGoodsTypeState(1);
           goodsTypeDao.updateGoodsTypeState(goodsType);
       }

        goodsTypeDao.saveType(goodsTypeName,pId);


    }

    /**
     * 删除分类
     * @param goodsTypeId
     */
    @Override
    public ServiceVO deleteType(Integer goodsTypeId) {

        GoodsType goodsType = goodsTypeDao.getById(goodsTypeId);

        if(goodsType.getGoodsTypeState()==1){
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS);
        }

      Integer count=  goodsTypeDao.getCountByPId(goodsType.getPId());

        if (count >= 2) {
            goodsTypeDao.deleteType(goodsTypeId);
        }else{
            GoodsType pGoodsType= goodsTypeDao.getById(goodsType.getPId());

            pGoodsType.setGoodsTypeState(0);

            goodsTypeDao.updateGoodsTypeState(pGoodsType);

            goodsTypeDao.deleteType(goodsTypeId);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}

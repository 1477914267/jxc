package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> pageGoodsList(Long page, Long rows, String codeOrName, String goodsTypeId) {

        page = (page - 1) * rows;

        Map<String, Object> map = new HashMap<>();

        map.put("page", page);
        map.put("rows", rows);
        map.put("codeOrName", codeOrName);
        map.put("goodsTypeId", goodsTypeId);

        List<Goods> goodsList = goodsDao.pageGoodsList(map);

        goodsList
                .stream()
                .forEach(item -> {

                    item.setSaleTotal(goodsDao.getSaleCount(item.getGoodsId()));
                });


        Integer goodsTotal = goodsList.size();

        Map<String, Object> goodsMap = new HashMap<>();

        goodsMap.put("total", goodsTotal);

        goodsMap.put("rows", goodsList);

        return goodsMap;


    }

    /**
     * 分页查询商品信息
     *
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Long page, Long rows, String goodsName, Long goodsTypeId) {

        page = (page - 1) * rows;

        Map<String, Object> map = new HashMap<>();

        map.put("page", page);
        map.put("rows", rows);
        map.put("goodsName", goodsName);
        map.put("goodsTypeId", goodsTypeId);

        List<Goods> goodsList = goodsDao.getGoodsList(map);

        Integer goodsTotal = goodsList.size();

        Map<String, Object> goodsMap = new HashMap<>();

        goodsMap.put("total", goodsTotal);

        goodsMap.put("rows", goodsList);

        return goodsMap;


    }

    /**
     * 商品添加或修改
     * @param goods
     * @param goodsId
     */
    @Override
    public void saveOrUpdate(Goods goods, Integer goodsId) {

        if (goodsId == null) {

            goodsDao.saveGoods(goods);
        }else{
            goods.setGoodsId(goodsId);
            goodsDao.update(goods);
        }

    }


    /**
     * 删除商品信息
     *
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {

       Goods goods= goodsDao.getById(goodsId);

        if (goods.getState() == 2) {
          return   new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);

        }else {

            goodsDao.delete(goodsId);
            return  new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }



    }

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */

    @Override
    public Map<String, Object> getNoInventoryQuantity(Long page, Long rows, String nameOrCode) {

        page = (page - 1) * rows;

        Map<String, Object> map = new HashMap<>();

        map.put("page", page);
        map.put("rows", rows);
        map.put("nameOrCode", nameOrCode);

        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(map);

        Integer goodsTotal = goodsList.size();

        Map<String, Object> goodsMap = new HashMap<>();

        goodsMap.put("total", goodsTotal);

        goodsMap.put("rows", goodsList);

        return goodsMap;
    }

    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Long page, Long rows, String nameOrCode) {

        page = (page - 1) * rows;

        Map<String, Object> map = new HashMap<>();

        map.put("page", page);
        map.put("rows", rows);
        map.put("nameOrCode", nameOrCode);

        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(map);

        Integer goodsTotal = goodsList.size();

        Map<String, Object> goodsMap = new HashMap<>();

        goodsMap.put("total", goodsTotal);

        goodsMap.put("rows", goodsList);

        return goodsMap;

    }

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {

        Goods goods = goodsDao.getById(goodsId);

        goods.setGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);

        goodsDao.update(goods);

    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {

       List<Goods> goodsList=goodsDao.listAlarm();

       Map<String, Object> map = new HashMap<>();

       map.put("rows",goodsList);
        return map;
    }


}

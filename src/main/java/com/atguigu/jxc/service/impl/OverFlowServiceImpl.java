package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.*;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.OverFlowService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ClassName: DamageService
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 21:26
 * @Version 1.0
 */
@Service
public class OverFlowServiceImpl implements OverFlowService {

    @Autowired
    OverFlowListDao overFlowListDao;

    @Autowired
    OverFlowListGoodsDao overFlowListGoodsDao;

    @Autowired
    UserDao userDao;


    /**
     * 保存报损单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     * @param user
     */
    @Override
    public void save(OverflowList overflowList, String overflowListGoodsStr, User user) {

        //将字符串转换为对象
        Type type = new TypeToken<List<OverflowListGoods>>() {
        }.getType();
        List<OverflowListGoods> list = new Gson().fromJson(overflowListGoodsStr, type);

        overflowList.setUserId(user.getUserId());

        overFlowListDao.save(overflowList);

        list.stream()
                .forEach(item ->
                        item.setOverflowListId(overflowList.getOverflowListId())
                );
        overFlowListGoodsDao.batchSave(list);


    }

    /**
     * 报溢单信息查询
     * @param sTime
     * @param eTime
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> getoverFlowList(String sTime, String eTime, User user) {

        List<OverflowList> overFlowList = overFlowListDao.getOverFlowList(sTime, eTime);

        overFlowList.stream()
                .forEach(item ->
                        item.setTrueName(userDao.getUserById(item.getUserId()).getTrueName())
                );

        Map<String, Object> map = new HashMap<>();

        map.put("rows", overFlowList);

        return map;

    }

    /**
     * 报溢单商品信息查询
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        List<OverflowListGoods> overFlowGoodsList=  overFlowListGoodsDao.goodsList(overflowListId);

        Map<String, Object> map = new HashMap<>();

        map.put("rows", overFlowGoodsList);

        return map;



    }
}

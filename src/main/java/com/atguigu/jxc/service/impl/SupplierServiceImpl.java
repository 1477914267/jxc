package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ClassName: SupplierServiceImpl
 * Package: com.atguigu.jxc.service.impl
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 15:49
 * @Version 1.0
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;


    /**
     * 分页查询供货商信息
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {

        page = (page - 1) * rows;

        Map<String, Object> ParamMap = new HashMap<>();

        ParamMap.put("page", page);
        ParamMap.put("rows", rows);
        ParamMap.put("supplierName", supplierName);

        List<Supplier> supplierList = supplierDao.getSupplierList(ParamMap);

        HashMap<String, Object> supplierMap = new HashMap<>();

        supplierMap.put("rows", supplierList);
        supplierMap.put("total", supplierList.size());

        return supplierMap;
    }

    /**
     * 供应商添加或修改
     *
     * @param supplier
     * @param supplierId
     * @return
     */
    @Override
    public void saveOrUpdate(Supplier supplier, Integer supplierId) {

        if (supplierId == null) {

            supplierDao.save(supplier);

        } else {
            supplier.setSupplierId(supplierId);
            supplierDao.update(supplier);
        }
    }

    /**
     * 删除供应商
     *
     * @param idStr
     */
    @Override
    public void remove(String idStr) {

        List<Integer> ids = Arrays.stream(idStr.split(",")).map(item -> Integer.parseInt(item)).collect(Collectors.toList());

        supplierDao.batchDel(ids);

    }
}

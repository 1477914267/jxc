package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * ClassName: SupplierService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 15:49
 * @Version 1.0
 */
public interface SupplierService {
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    void saveOrUpdate(Supplier supplier, Integer supplierId);

    void remove(String ids);

}

package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * ClassName: CustomerService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 17:35
 * @Version 1.0
 */
public interface CustomerService {
    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Customer customer, Integer customerId);

    void remove(String ids);

}

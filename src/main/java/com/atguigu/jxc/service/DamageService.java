package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;

import java.util.Map;

/**
 * ClassName: DamageService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 21:26
 * @Version 1.0
 */
public interface DamageService {
    void save(DamageList damageList, String damageListGoodsStr, User user);


    Map<String, Object> getDamageList(String sTime, String eTime, User user);

    Map<String, Object> goodsList(Integer damageListId);
}

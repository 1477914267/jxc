package com.atguigu.jxc.service;

import java.util.Map;

/**
 * ClassName: UnitService
 * Package: com.atguigu.jxc.service
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 18:11
 * @Version 1.0
 */
public interface UnitService {
    Map<String, Object> unitList();
}

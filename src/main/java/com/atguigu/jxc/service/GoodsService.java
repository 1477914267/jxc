package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> pageGoodsList(Long page, Long rows, String codeOrName, String goodsTypeId);

    Map<String, Object> getGoodsList(Long page, Long rows, String goodsName, Long goodsTypeId);

    void saveOrUpdate(Goods goods, Integer goodsId);

    ServiceVO delete(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Long page, Long rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Long page, Long rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);


    Map<String, Object> listAlarm();


}

package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.vo.GoodsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> pageGoodsList(Map<String, Object> map);


    Integer getSaleCount(Integer goodsId);

    List<Goods> getGoodsList(Map<String, Object> map);

    void saveGoods(Goods goods);

    void update(Goods goods);

    Goods getById(Integer goodsId);

    void delete(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Map<String, Object> map);

    List<Goods> getHasInventoryQuantity(Map<String, Object> map);

    List<Goods> listAlarm();

}

package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * ClassName: UnitDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 18:14
 * @Version 1.0
 */
public interface UnitDao {


    List<Unit> unitList();

}

package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 15:53
 * @Version 1.0
 */
public interface SupplierDao {


    List<Supplier> getSupplierList(Map<String, Object> map);

    void save(Supplier supplier);

    void update(Supplier supplier);

    void batchDel(List<Integer> idList);
}

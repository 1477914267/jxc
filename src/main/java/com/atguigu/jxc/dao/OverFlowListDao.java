package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ClassName: DamageListDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/12 9:01
 * @Version 1.0
 */
public interface OverFlowListDao {


    void save(OverflowList overflowList);

    List<OverflowList> getOverFlowList(@Param("sTime") String sTime, @Param("eTime")String eTime);
}

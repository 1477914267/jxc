package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

/**
 * ClassName: DamageListGoods
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/12 9:02
 * @Version 1.0
 */
public interface DamageListGoodsDao {
    void batchSave(List<DamageListGoods> list);

    List<DamageListGoods> goodsList(Integer damageListId);
}

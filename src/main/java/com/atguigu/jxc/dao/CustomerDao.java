package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerDao
 * Package: com.atguigu.jxc.dao
 * Description:
 *
 * @Author 23-02-01-LT
 * @Create 2023/9/11 17:39
 * @Version 1.0
 */
public interface CustomerDao {

    List<Customer> getCustomerList(Map<String, Object> map);

    void save(Customer customer);

    void update(Customer customer);

    void batchDel(List<Integer> idList);


}

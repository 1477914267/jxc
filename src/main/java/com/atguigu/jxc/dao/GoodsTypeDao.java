package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void saveType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    void deleteType(Integer goodsTypeId);

    GoodsType getById(Integer pId);

    Integer getCountByPId(Integer pId);
}
